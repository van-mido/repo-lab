<?php

	require_once 'chuck.php';

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Chuck Norris API</title>
    <style>li { list-style: none; }</style>
</head>
<body>

       <header>Este es el encabezado</header> 
        <ul>
            <li><strong><?= $chuck['created_at']; ?></strong></li>
            <li><img src="<?= $chuck['icon_url']; ?>" alt=""></li>
            <li><?= $chuck['id']; ?></li>
            <li><?= $chuck['updated_at']; ?></li>
            <li><?= $chuck['url']; ?></li>
            <li><?= $chuck['value']; ?></li>
        </ul>
<div class="sidebar">Sidebar</div>
        <script src="app.js"></script>
    
</body>
</html>
